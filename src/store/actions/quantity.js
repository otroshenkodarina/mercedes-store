import * as QUANTITY from '../constans/quantity';

export function setQuantity(data){
    return function (dispatch){
        return dispatch({
            type: QUANTITY.QUANTITY_SET,
            data,
        })
    }
}