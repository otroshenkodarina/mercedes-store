import * as ACTIVE_MODAL from '../constans/isActiveModal';

export function setIsActiveModal(data){
    return function (dispatch){
        return dispatch({
            type: ACTIVE_MODAL.SET_ACTIVE_MODAL,
            data,
        })
    }
}