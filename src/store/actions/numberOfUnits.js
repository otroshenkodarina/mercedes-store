import * as NUMBER_OF_UNITS from "../constans/numberOfUnits";

export function setNumberOfUnits(data){
    return function (dispatch){
        return dispatch({
            type: NUMBER_OF_UNITS.SET_NUMBER_OF_UNITS,
            data,
        })
    }
}

