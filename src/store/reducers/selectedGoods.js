import * as SELECTED_GOODS from '../constans/selectedGoods'

const initialState = {
    data: JSON.parse(localStorage.getItem('selectedGoods')) || [],
    isLoading: true,
    error: null
}

export default function GoodsInCart(state = initialState, action) {
    switch (action.type) {
        case SELECTED_GOODS.SELECTED_GOODS: {
            return {
                ...state,
                data: action.data,
                isLoading: false
            }
        }

        default:
            return state
    }
}