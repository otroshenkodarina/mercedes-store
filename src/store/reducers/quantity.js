import * as QUANTITY from '../constans/quantity';

const initialState = {
    data: JSON.parse(localStorage.getItem('goodsInCart'))?.length || 0,
    isLoading: true,
    error: null
}

export default function Quantity(state = initialState, action) {
    switch (action.type) {
        case QUANTITY.QUANTITY_SET: {
            return {
                ...state,
                data: action.data,
                isLoading: false
            }
        }

        default:
            return state
    }
}