import React from 'react';
import {Route, Switch} from 'react-router-dom'
import Error404 from "../pages/Error404/Error404";
import Selected from "../pages/Selected/Selected";
import Cart from "../pages/Cart/Cart";
import App from "../App";

const AppRoutes = () => {
    return (
        <div>
            <Switch>
                <Route exact path='/'><App /></Route>
                <Route exact path='/selected'><Selected /></Route>
                <Route exact path='/cart'><Cart /></Route>
                <Route path='*'><Error404/></Route>
            </Switch>
        </div>
    );
};

export default AppRoutes;