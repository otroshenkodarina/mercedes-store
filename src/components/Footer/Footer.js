import React from 'react';
import './Footer.scss'

const Footer = () => {

    return (
        <div className='footer'>
            DREAMS COME TRUE
        </div>
    );

}

export default Footer;