import React from 'react';
import './Header.scss'
import PropTypes from 'prop-types'
import {NavLink, Link} from "react-router-dom";

const Header = () => {
    return (
        <div className='header'>
            <Link to='/' className='header-logo'>MERCEDES-BENZ</Link>

            <div className='header-nav'>
                <NavLink className='header-cart' exact to='/selected' activeClassName = 'selected'>Selected products</NavLink>
                <NavLink className='header-cart' exact to='/cart' activeClassName = 'selected'>Cart of products: <span>{JSON.parse(localStorage.getItem('goodsInCart'))?.length || 0}</span></NavLink>
            </div>
        </div>
    );

}

Header.propTypes = {
    quantity: PropTypes.number,
    showCart: PropTypes.func,
    showSelected: PropTypes.func,
}

Header.defaulltProps = {
    quantity: 0
}

export default Header;