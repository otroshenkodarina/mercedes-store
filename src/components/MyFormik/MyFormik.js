import React from 'react';
import {Formik, Form, Field} from 'formik'
import MyInput from "../MyInput/MyInput";
import * as yup from 'yup'
import './MyFormik.scss'
import {useDispatch, useSelector} from "react-redux";
import * as cartActions from "../../store/actions/goodsInCart";

const IS_REQUIRED = 'This field is required'

const schema = yup.object().shape({
    name: yup.string().required(IS_REQUIRED),
    surname: yup.string().required(IS_REQUIRED),
    age: yup.number().required(IS_REQUIRED),
    address: yup.string().required(IS_REQUIRED),
    phone: yup.number().required(IS_REQUIRED).min(10, 'Enter your phone'),
})

// const MyNumberFormat = (props) => {
//     const {field, form, ...rest} = props
//     const {name} = field
//     return (
//         <>
//             <NumberFormat
//                 {...rest}
//                 {...field}
//                 value=''
//                 onValueChange={values => console.log(values)}
//                 format='(###)###-##-##'
//             />
//             {form.touched[name] && form.errors[name] && <div className='error'>{form.errors[name]}</div>}
//         </>
//     )
// }


const MyFormik = () => {
    const goodsState = useSelector(state => state.goods.data)
    const cartState = useSelector(state => state.goodsInCart.data)
    const dispatch = useDispatch()

    const handleSubmit = (values) => {
        const purchasesArr = goodsState.filter(item => cartState.includes(item.vendorCode))
        purchasesArr.map(item => console.log(item.name, item.price))

        console.log(values)

        dispatch(cartActions.setGoodsInCart([]))
        localStorage.setItem('goodsInCart', JSON.stringify([]))
    }

    return (

        <Formik
            initialValues={{name: '', surname: '', age: '', address: '', phone: ''}}
            onSubmit={handleSubmit}
            validationSchema={schema}
        >
            {({errors, touched}) => {
                return (
                    <Form className='form'>
                        <p className='title'>To buy a Mercedes fill the form below</p>
                        <Field
                            className='input'
                            component={MyInput}
                            name='name'
                            type='text'
                            placeholder='enter your name'
                        />
                        <Field
                            className='input'
                            component={MyInput}
                            name='surname'
                            type='text'
                            placeholder='enter your surname'
                        />
                        <Field
                            className='input'
                            component={MyInput}
                            name='age'
                            type='number'
                            placeholder='enter your age'
                        />
                        <Field
                            className='input'
                            component={MyInput}
                            name='address'
                            type='text'
                            placeholder='enter your address'
                        />
                        <Field
                            className='input'
                            component={MyInput}
                            name='phone'
                            type='number'
                            placeholder='enter your phone'
                        />
                        <button className='btn' type='submit'>Checkout</button>
                    </Form>
                )
            }}
        </Formik>
    );
};

export default MyFormik;

