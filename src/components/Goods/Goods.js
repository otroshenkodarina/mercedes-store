import React from 'react';
import './Goods.scss'
import Button from "../Button/Button";
import Icons from "../Icons/Icons";
import PropTypes from 'prop-types'
import * as selectedActions from "../../store/actions/selectedGoods";
import {useDispatch, useSelector} from "react-redux";

const Goods = ({
                   name,
                   price,
                   img,
                   vendorCode,
                   color,
                   onClick,
                   clickStarFunc,
                   deleteFromCart,
                   closeBtn,
                   isShowedButtons,
               }) => {

    const selectedGoodsState = useSelector(state => state.selectedGoods)
    const cartState = useSelector(state => state.goodsInCart)

    const dispatch = useDispatch();

    const numberOfUnits = cartState.data.filter(item => item === vendorCode).length

    const deleteFromSelectedFunc = (cartID) => {
        const idIndex = selectedGoodsState?.data.indexOf(cartID)

        if (idIndex !== -1) {
            selectedGoodsState?.data.splice(idIndex, 1)
        }

        localStorage.setItem('selectedGoods', JSON.stringify(selectedGoodsState.data))
        dispatch(selectedActions.setSelectedGoods(selectedGoodsState.data))
    }


    return (
        <div className='goods-card'>
            <div className='goods-card__header'>
                <p className='goods-card__name'>{name}</p>
            </div>

            <img className='goods-card__img' src={img} alt='sorry'/>

            <div className='goods-card__desc'>
                <p className='goods-card__price'>Price: {price}</p>
                <p className='goods-card__vendorCode'>Vendor code: {vendorCode}</p>
                <p className='goods-card__color'>Color: {color}</p>
            </div>

            {
                numberOfUnits !== 0 &&
                <p className='goods-card__number'>Quantity already in the cart: {numberOfUnits} item</p>
            }

            <div className='goods-card__buttons'>
                {isShowedButtons && <>
                    <Button
                        text='Add to cart'
                        onClick={onClick}
                        backgroundColor='darkgreen'
                    />
                    <Icons
                        clickStar={clickStarFunc}
                        vendorCode={vendorCode}
                        deleteFromSelected={() => deleteFromSelectedFunc(vendorCode)}
                    />
                </>}
                {closeBtn && <Button
                    text='Delete from cart'
                    onClick={deleteFromCart}
                    backgroundColor='darkgreen'
                />}
            </div>
        </div>
    );
}

Goods.propTypes = {
    name: PropTypes.string,
    price: PropTypes.string,
    img: PropTypes.string,
    vendorCode: PropTypes.string,
    color: PropTypes.string,
    onClick: PropTypes.func,
    selectedGoods: PropTypes.array,
    deleteFromSelectedFunc: PropTypes.func,
    clickStarFunc: PropTypes.func,
}

Goods.defaultProps = {
    name: '',
    price: '',
    img: '',
    vendorCode: '',
    color: '',
    selectedGoods: [],
}

export default Goods;