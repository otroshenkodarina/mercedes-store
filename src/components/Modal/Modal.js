import React from 'react';
import './Modal.scss'
import PropTypes from 'prop-types'

const Modal = ({header, closeButton, text, actions, handleCloseButton, closeModal}) => {

    return (
        <div className='modal-container' onClick={handleCloseButton}>
            <div className='modal'>
                <div className='modal__header'>
                    <h3 className='modal__header--text'>{header}</h3>
                    {
                        closeButton
                        &&
                        <svg onClick={closeModal} className='modal__close' viewBox="0 0 32 32"
                             xmlns="http://www.w3.org/2000/svg">
                            <title/>
                            <g id="cross">
                                <line className="cls-1" x1="7" x2="25" y1="7" y2="25"/>
                                <line className="cls-1" x1="7" x2="25" y1="25" y2="7"/>
                            </g>
                        </svg>}
                </div>
                <p className='modal__text'>{text}</p>
                {actions}
            </div>
        </div>
    )

}

Modal.propTypes = {
    header: PropTypes.string,
    closeButton: PropTypes.bool,
    handleCloseButton: PropTypes.func,
    closeModal: PropTypes.func,
}

Modal.defaultProps = {
    header: '',
}

export default Modal;