import React, {useEffect} from 'react';
import './Selected.scss'
import {useDispatch, useSelector} from "react-redux";
import Goods from "../../components/Goods/Goods";
import Header from "../../components/Header/Header";
import Footer from "../../components/Footer/Footer";
import Button from "../../components/Button/Button";
import Modal from "../../components/Modal/Modal";
import * as quantityActions from "../../store/actions/quantity";
import * as addToCartIdActions from "../../store/actions/addToCartId";
import * as setActiveModal from '../../store/actions/isActiveModal'
import * as goodsActions from "../../store/actions/goods";

const Selected = () => {
    const dispatch = useDispatch();

    const goodsState = useSelector(state => state.goods)
    const cartState = useSelector(state => state.goodsInCart.data)
    const selectedState = useSelector(state => state.selectedGoods)
    const quantity = useSelector(state => state.quantity.data)
    const addToCartID = useSelector(state => state.addToCartId.data)
    const isActiveModal = useSelector(state => state.isActiveModal.data)

    useEffect(() => {
        dispatch(goodsActions.setGoods())
    }, [])// eslint-disable-line react-hooks/exhaustive-deps

    const result = goodsState.data.filter(item => selectedState.data.includes(item.vendorCode))

    const handleCloseButton = (e) => {
        if (e.target.className === 'modal-container') {
            dispatch(setActiveModal.setIsActiveModal(false))
        }
    }

    const addToCart = () => {
        cartState.push(addToCartID)
        localStorage.setItem('goodsInCart', JSON.stringify(cartState))

        dispatch(quantityActions.setQuantity(quantity + 1));
        dispatch(setActiveModal.setIsActiveModal(false))
    }

    const openCart = (cartId) => {
        dispatch(setActiveModal.setIsActiveModal(true))
        dispatch(addToCartIdActions.setCartId(cartId))
    }

    if (selectedState?.data.length <= 0) {
        return (
            <div className='selected-page'>
                <div className='selected-empty'>
                    <Header/>
                    <p className='no-items'>No items had been added</p>
                </div>
            </div>
        )
    }

    return (
        <div className='selected-page'>
            <Header/>
            <div className='selected-list'>
                {result?.map((item, key) => <Goods
                    key={key}
                    name={item.name}
                    price={item.price}
                    img={item.source}
                    vendorCode={item.vendorCode}
                    color={item.color}
                    isShowedButtons={true}
                    onClick={() => openCart(item.vendorCode)}
                />)
                }
            </div>

            {
                isActiveModal
                && <Modal
                    closeModal={() => dispatch(setActiveModal.setIsActiveModal(false))}
                    handleCloseButton={handleCloseButton}
                    header='PRODUCT CART'
                    closeButton={true}
                    text='Do you want to add this product to cart?'
                    actions={
                        <div className='modal__buttons'>
                            <Button
                                text='Yes, sure'
                                onClick={addToCart}
                                backgroundColor='darkseagreen'
                            />
                            <Button
                                text='Cancel'
                                onClick={() => dispatch(setActiveModal.setIsActiveModal(false))}
                                backgroundColor='darksalmon'
                            />
                        </div>
                    }
                />
            }


            <Footer/>
        </div>
    )
};

export default Selected;